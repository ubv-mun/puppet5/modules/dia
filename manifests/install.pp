# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dia::install
class dia::install {
  package { $dia::package_name:
    ensure => $dia::package_ensure,
  }
}
